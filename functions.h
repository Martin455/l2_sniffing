/*
* CDP LLDP monitor
* Pomocne funkce aplikace
**/
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <cstring>
#include <string>

class Functions
{
public:
	static unsigned convertBufferToInt(const unsigned char *buffer, unsigned num_byte);
	static std::string convertBufferToString(const unsigned char *buffer, unsigned num_byte);
	static void convertIntToBuffer(unsigned char *buffer,int number, int byte);
	static void print_title();
	static std::string print_help();
};


#endif