//CDP LLDP monitor

#include "functions.h"
#include <iostream>
//prevod bufferu na cislo o velikosti zadanych bytu
unsigned Functions::convertBufferToInt(const unsigned char *buffer, unsigned num_byte)
{
	unsigned result = 0x0000;
	num_byte -= 1;
	int index = 0;
	while (num_byte!=0)
	{
		unsigned tmp = num_byte*8;

		result = result | (buffer[index] << tmp);
		num_byte -= 1;
		index++;
	}
	result = result | buffer[index];
	return result;
}
//prevod buffer na string o zadanych bytu
std::string Functions::convertBufferToString(const unsigned char *buffer, unsigned num_byte)
{
	char *tmp_buffer = new char[num_byte+1];
	std::memcpy(tmp_buffer,buffer,num_byte);
	tmp_buffer[num_byte] = '\0';
	std::string result(tmp_buffer);
	delete [] tmp_buffer;

	return result;	
}
//prevod int do bufferu o zadanych bytech
void Functions::convertIntToBuffer(unsigned char *buffer,int number, int byte)
{
	byte -= 1;
	int j = 0;
	u_char *ptr = (u_char*)&number;
	for(int i = byte; i >= 0; i--)
	{
		buffer[j] = ptr[i];
		j++;
	}
}

void Functions::print_title()
{	//vypis nazvu protokolu
	std::cout << "###################################################\n"
				 "##...............................................##\n"
				 "##   ##  ###   ###         #    #    ###   ###   ##\n"
				 "##  #  # #  #  #  #        #    #    #  #  #  #  ##\n"
				 "##  #    #   # ###    ###  #    #    #   # ###   ##\n"
				 "##  #  # #  #  #           #    #    #  #  #     ##\n"
				 "##   ##  ###   #           #### #### ###   #     ##\n"
				 "##-----------------------------------------------##\n";
}
//vraceni napovedy vypisujici se pri chybach
std::string Functions::print_help()
{
	return std::string("Application for monitoring CDP and LLDP protocols\n"
	"Using: ./myL2monitor -i <interface> {--send-hello {--ttl <sekundy>} {--duplex [full|half]} {--software-version <version>} {--device-id <identifier>} {--platform <platform>} {--port-id <interface>} {--capabilities <integer>} {--address <IPv4>}}");
}