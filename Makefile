#Makefile for myL2monitor
#Autor Martin Prajka

CC=g++
CFLAGS=-std=c++11 -g -Wall -Wextra -pedantic
LIBS=-lpcap
OBJECTFILES=arg_parser.o myL2monitor.o functions.o packet_work.o

all: myL2monitor

functions.o: functions.cpp
	$(CC) $(CFLAGS) -c functions.cpp -o functions.o

packet_work.o: packet_work.cpp
	$(CC) $(CFLAGS) -c packet_work.cpp -o packet_work.o $(LIBS)

arg_parser.o: arg_parser.cpp
	$(CC) $(CFLAGS) -c arg_parser.cpp -o arg_parser.o $(LIBS)

myL2monitor.o: myL2monitor.cpp
	$(CC) $(CFLAGS) -c myL2monitor.cpp -o myL2monitor.o $(LIBS)

myL2monitor: $(OBJECTFILES)
	$(CC) $(CFLAGS) -pthread -o myL2monitor $(OBJECTFILES) $(LIBS)

clean:
	rm -f myL2monitor *.o

pack:
	tar -cf xprajk00.tar *.cpp Makefile *.h *.pdf README
