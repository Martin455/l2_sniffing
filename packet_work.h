//CDP LLDP monitor

#ifndef PACKET_WORK_H
#define PACKET_WORK_H

//pro pomocne funkce prace se stringem
#include <string>
//uzavreni soketu
#include <unistd.h>
//pro ziskani dat
#include "arg_parser.h"
#include "functions.h"

//sitove knihovny
#include <pcap.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/types.h> 
#include <net/if.h>

using std::string;

//sitove konstanty (pouzivaji se i v modulu myL2monitor.cpp)
const uint16_t LLDP = 0x88cc; //LLDP number type protokol pro oddeleni LLDP od CDP
const uint32_t CDP_OFFSET = 22; //offset pro CDP protokol
const uint32_t IP_OFFSET = 14;

//struktury
struct L_link_control {	//pro ulozeni dat logical link control
	u_char dsap;
	u_char ssap;
	u_char cntl;
	u_char org_code[3];
	unsigned short pid;
};
struct Cdp_head {	//data pro hlavicku
	u_char version;
	u_char ttl;
	ushort checksum;
};

struct Cdp_data {	//tlv pocatecni data
	short type;
	short length;
};

//deklarace funkci
void createCDP_packet(ArgParser &data);
void createLLDP_packet(ArgParser &data);
void destroyPacket(u_char *);
//pomocne CDP funkce
uint putCDP_str_data(u_char *packet, unsigned offset,int type, string data);
u_int16_t checksumCounter(u_char *data, uint length);


void getIp4(u_char *buffer, std::string str);
void getMacAddr(u_char *,const char *interface);

#endif