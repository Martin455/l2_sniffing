//CDP LLDP monitor

#include "packet_work.h"

extern u_char *packet_cdp;
extern u_char *packet_lldp;
extern uint len_cdp;
extern uint len_lldp;
//Vytvoreni LLDP paketu
void createLLDP_packet(ArgParser &data)
{
	u_char *packet = nullptr;
	u_char my_eth[6];
	u_char eth_dest[6] = {0x01,0x80,0xc2,0x00,0x00,0x0e};

	unsigned offset = 0;
	uint length_packet = 14;
	length_packet += 7 + 2; //chassis mac addres
	length_packet += 4; //ttl 
	length_packet += 6; //capabilities
	length_packet += 2; //end lldp
	length_packet += data.GetInterface().size() + 3;	//interface name
	length_packet += data.GetSoftVersion().size() + 2; //system description
	length_packet += data.GetDeviceID().size() + 2; 	//system name
	length_packet += data.GetPortID().size() + 2;	//port description
	length_packet += 14; //ip address

	packet = new u_char[length_packet];
	std::memset(packet,0,length_packet);
	getMacAddr(my_eth,data.GetInterface().c_str());

	std::memcpy(packet,eth_dest,6);
	offset += 6;
	std::memcpy(packet+offset,my_eth,6);
	offset += 6;
	Functions::convertIntToBuffer(packet+offset,LLDP,2);
	offset +=2;

	(packet+offset)[0] = 0x0001 << 1;
	(packet+offset)[1] = 7;
	(packet+offset)[2] = 4;
	offset+=3;
	std::memcpy(packet+offset,my_eth,6);
	offset+=6;

	(packet+offset)[0] = 0x0002 << 1;
	(packet+offset)[1] = data.GetInterface().size() + 1;
	(packet+offset)[2] = 5;
	offset += 3;
	std::memcpy(packet+offset,data.GetInterface().c_str(), data.GetInterface().size());
	offset += data.GetInterface().size();

	(packet+offset)[0] = 0x0003 << 1;
	(packet+offset)[1] = 2;
	offset += 2;
	Functions::convertIntToBuffer(packet+offset, data.GetTTL_seconds(),2);
	offset += 2;	

	(packet+offset)[0] = 0x0005 << 1;
	(packet+offset)[1] = data.GetDeviceID().size();
	offset += 2;
	std::memcpy(packet+offset, data.GetDeviceID().c_str(),data.GetDeviceID().size());
	offset += data.GetDeviceID().size();

	(packet+offset)[0] = 0x0006 << 1;
	(packet+offset)[1] = data.GetSoftVersion().size();
	offset += 2;
	std::memcpy(packet+offset, data.GetSoftVersion().c_str(),data.GetSoftVersion().size());
	offset += data.GetSoftVersion().size();

	(packet+offset)[0] = 0x0004 << 1;
	(packet+offset)[1] = data.GetPortID().size();
	offset += 2;
	std::memcpy(packet+offset, data.GetPortID().c_str(),data.GetPortID().size());
	offset += data.GetPortID().size();

	(packet+offset)[0] = 0x0007 << 1;
	(packet+offset)[1] = 4;
	offset += 2;

	int capab = data.GetCapabilities();
	if (capab==16)
	{
		Functions::convertIntToBuffer(packet+offset, 1,2);
		offset += 2;
		Functions::convertIntToBuffer(packet+offset, 1,2);
		offset += 2;
	}
	else
	{
		Functions::convertIntToBuffer(packet+offset, capab,2);
		offset += 2;
		Functions::convertIntToBuffer(packet+offset, capab,2);
		offset += 2;	
	}
	(packet+offset)[0] = 0x0008 << 1;
	(packet+offset)[1] = 12;
	(packet+offset)[2] = 5;
	(packet+offset)[3] = 1;
	offset += 4;
	getIp4(packet+offset,data.GetAddress());
	offset += 4;
	(packet+offset)[0] = 2;
	offset++;
	Functions::convertIntToBuffer(packet+offset,5,4);
	offset += 7;	//zahrnut jeste jeden nulovy byte + endLLPD

	len_lldp = length_packet;
	packet_lldp = packet;
}
//Vytvoreni CDP paketu
void createCDP_packet(ArgParser &data)
{
	u_char *packet;
	u_char my_eth[6];
	u_char eth_dest[6] = {0x01,0x00,0x0c,0xcc,0xcc,0xcc};	//nastaveni CDP cilove adresy
	L_link_control *header_l;	//logical link control data
	Cdp_head *cdp_header;	//cdp header data
	unsigned offset = 0;	//offset pro posuv v paketu

	//vypocet delky paketu
	unsigned short length_packet = 14 + sizeof(L_link_control) + sizeof(Cdp_head);
	length_packet += data.GetDeviceID().size() + 4;
	length_packet += data.GetSoftVersion().size() + 4;
	length_packet += data.GetPlatform().size() + 4;
	length_packet += data.GetPortID().size()+4;
	//ip adresa
	length_packet += 17;
	//capabilities
	length_packet += 8;
	//duplex
	length_packet += 5;

	getMacAddr(my_eth,data.GetInterface().c_str());	//zjisteni mac adresy na rozhrani

	//alokace paketu a vynulovani pameti
	packet = new u_char[length_packet];
	std::memset(packet,0,length_packet);

	//kopirovani ethernet hlavicky
	std::memcpy(packet,eth_dest,6);
	offset += 6;
	std::memcpy(packet+offset,my_eth,6);
	offset += 6;
	Functions::convertIntToBuffer(packet+offset,length_packet,2);
	offset += 2;
	//nakopirovani logical link control dat
	header_l = (L_link_control *)(packet+offset);
	header_l->dsap = 0xaa;
	header_l->ssap = 0xaa;
	header_l->cntl = 0x03;
	header_l->org_code[2] = 0x0c;
	header_l->pid = 0x0020;	//kvuli cteni paketu prehozeno 0x2000 se objevi ve vyslednem paketu
	offset += sizeof(L_link_control);	//posuv

	//nakopirovani cdp header dat
	cdp_header = (Cdp_head *)(packet+offset);
	cdp_header->version = 2;
	cdp_header->ttl = (u_char)data.GetTTL_seconds();
	//vynulovani checksum
	Functions::convertIntToBuffer((u_char*)&cdp_header->checksum,0,2);
	offset += sizeof(Cdp_head);	//posuv

	//Kopirovani CDP dat do paketu
	//pokazde typ delka data a posuv o urcity pocet bytu
	offset = putCDP_str_data(packet,offset,0x0001,data.GetDeviceID());
	offset = putCDP_str_data(packet,offset,0x0005,data.GetSoftVersion());
	offset = putCDP_str_data(packet,offset,0x0006,data.GetPlatform());
	offset = putCDP_str_data(packet,offset,0x0003,data.GetPortID());
	//kopirovani IP adresy
	Functions::convertIntToBuffer(packet+offset,0x0002,2);
	offset += 2;
	Functions::convertIntToBuffer(packet+offset,17,2);
	offset += 2;
	Functions::convertIntToBuffer(packet+offset,1,4);
	offset += 4;
	(packet+offset)[0] = 0x01;
	offset++;
	(packet+offset)[0] = 0x01;
	offset++;
	(packet+offset)[0] = 0xcc;
	offset++;
	Functions::convertIntToBuffer(packet+offset,4,2);
	offset+=2;
	getIp4(packet+offset,data.GetAddress());
	offset += 4;

	Functions::convertIntToBuffer(packet+offset,0x0004,2);
	offset+=2;
	Functions::convertIntToBuffer(packet+offset,8,2);
	offset += 2;
	Functions::convertIntToBuffer(packet+offset,data.GetCapabilities(),4);
	offset += 4;

	Functions::convertIntToBuffer(packet+offset,0x000b,2);
	offset += 2;
	Functions::convertIntToBuffer(packet+offset,5,2);
	offset += 2;
	if (data.isDuplexFull())
	{
		packet[offset] = 1;
	}
	else
	{
		packet[offset] = 0;
	}
	//soucet checksum
	ushort sum = checksumCounter(packet+CDP_OFFSET,length_packet-CDP_OFFSET);
	//nastaveni
	cdp_header->checksum=sum;

	len_cdp = length_packet;
	packet_cdp = packet;
}

//nahrani retezcovych dat do CDP paketu
uint putCDP_str_data(u_char *packet, unsigned offset,int type, string data)
{
	Cdp_data *cdp_data = (Cdp_data *)(packet + offset);
	Functions::convertIntToBuffer((u_char *)&cdp_data->type,type,2);
	Functions::convertIntToBuffer((u_char *)&cdp_data->length,data.size() + 4,2);
	offset += 4;

	std::memcpy(packet+offset,data.c_str(),data.size());

	return (offset + data.size());	
}
/*
*	Prevzita funkce z internetu pro vypocet checksum na cdp paketu
*	Aplikace scdp: pro odesilani CDP paketu: http://scdp.sourceforge.net
*	Copyright (C) 2001 Mikael Wedlin & Kent Engström, Linköping University.
*/
u_int16_t checksumCounter(u_char *data, uint length)
{
	u_int32_t sum = 0;
	u_int16_t *twoBytes;

	twoBytes = (u_int16_t *)data;
	while( length > 1)
	{
		sum += *twoBytes;
		twoBytes++;
		length -= 2;
	}

	if(length>0)
		sum = sum + ((*twoBytes & 0xFF) << 8);

	while (sum>>16)
		sum = (sum & 0xffff) + (sum >> 16);

	return ~sum;	//zmena indianu
}
/* Konec prevzite funkce */

void getIp4(u_char *buffer, std::string str)
{
	std::size_t index = str.find_first_of(".");	//nalezeni prvni tecky
	std::size_t last_index = 0;
	int i =0;
	while(index!=std::string::npos)	//dokud jsou tecky nahravej cislo
	{
		std::string number = str.substr(last_index,index-last_index);
		buffer[i] = (u_char)std::stoi(number,nullptr);
		i++; 
		str[index] = ' ';
		last_index = index + 1;
		index = str.find_first_of(".");
	}
	str = str.substr(last_index);
	buffer[i] = (u_char)std::stoi(str,nullptr); 	//zpracovani posledniho cisla za teckou
}

void getMacAddr(u_char *eth, const char *interface)
{	//pomocna funkce na ziskani MAC adresy z rozhrani
	struct ifreq buff;
	int socket_id;
	//nastaveni socketu
	socket_id = socket(PF_INET, SOCK_DGRAM, 0);
	std::memset(&buff,0,sizeof(buff));
	std::strcpy(buff.ifr_name,interface);	
	ioctl(socket_id, SIOCGIFHWADDR, &buff);
	close(socket_id);

	for (int i = 0; i < 6; ++i)	//ziskani adresy po bytu
	{
		eth[i] = (u_char)buff.ifr_hwaddr.sa_data[i];
	}
}

void destroyPacket(u_char *packet)
{	//uvolneni pameti paketu
	if(packet==nullptr)
		return;
	delete [] packet;
}
