/***
*
* ArgParser Class pro zpracovani argumentu a ulozeni dat zarizeni
* Autor: Martin Prajka
*/

#ifndef ARG_PARSER_H
#define ARG_PARSER_H
#include <iostream>
#include <stdexcept>
#include <sys/utsname.h>

const std::string EMPTYSTRING("");

class ArgParser
{
private:
	int count_arg;
	char **array_arg;
	struct Data {
		bool send_hello;
		unsigned int ttl = 0;
		unsigned int duplex = 0;
		int capabilities = 0;
		std::string software_version = EMPTYSTRING;
		std::string device_id = EMPTYSTRING;
		std::string platform = EMPTYSTRING;
		std::string port_id = EMPTYSTRING;
		std::string interface = EMPTYSTRING;
		std::string IPv4_address = EMPTYSTRING;
	};
	Data data;
	
	void SetDefaultValues();
	std::string GetVersionSoft_DeviceID(int vs_device_platform = 0); //0 whole data about version 1 only device ID 2 only platform
	std::string GetIpv4AddressOfInterface(const char* interface);
public:

	ArgParser(int argc,char**argv);
	void ParseData();
	std::string GetInterface();
	std::string GetSoftVersion();
	std::string GetPlatform();
	std::string GetDeviceID();
	std::string GetPortID();
	std::string GetAddress();
	unsigned int GetTTL_seconds();
	bool isDuplexFull();
	int GetCapabilities();
	bool isSendHello();
};

#endif