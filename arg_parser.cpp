/***
*
* Zdrojovy kod pro ArgParser
* Autor: Martin Prajka
*/

#include "arg_parser.h"
//libraries for getting Ipv4
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <cstring>

ArgParser::ArgParser(int argc, char** argv): count_arg(argc), array_arg(argv) { }

void ArgParser::ParseData()
{
	if(count_arg<3)
	{
		throw std::runtime_error("Too less argument(s)");
	}
	//prvni argument zjisteni
	if(std::string(array_arg[1])!="-i")	
	{
		throw std::runtime_error("Wrong argument interface: it must be '-i'");
	}
	data.interface = std::string(array_arg[2]);

	if(count_arg>3)
	{	//pokud je zadano vic potreba overit na send-hello
		if(std::string(array_arg[3])!="--send-hello")
		{
			throw std::runtime_error("Wrong third argument: it must be '--send-hello'");
		}
		data.send_hello = true;
		for(int i =4; i<count_arg;i+=2)	//zadani nepovinnych argumentu v jakemkoliv poradi
		{
			bool is_parameter =false;
			bool is_num_par = false;
			int parameter = 0;

			//overeni spravnosti
			std::string value("");
			if(std::string(array_arg[i])=="--ttl")
			{	
				is_parameter = true;
				is_num_par = true;
				parameter = 1;
			}
			else if (std::string(array_arg[i])=="--duplex")
			{
				is_parameter = true;
				parameter = 2;
			}
			else if (std::string(array_arg[i])=="--software-version")
			{
				is_parameter = true;
				parameter = 4;
			}
			else if (std::string(array_arg[i])=="--device-id")
			{
				is_parameter = true;
				parameter = 5;
			}
			else if (std::string(array_arg[i])=="--platform")
			{
				is_parameter = true;
				parameter = 6;
			}
			else if (std::string(array_arg[i])=="--port-id")
			{
				is_parameter = true;
				parameter = 7;
			}
			else if (std::string(array_arg[i])=="--capabilities")
			{
				is_parameter = true;
				is_num_par = true;
				parameter = 3;
			}
			else if (std::string(array_arg[i])=="--address")
			{
				is_parameter = true;
				parameter = 8;
			}
			//pokud parametr prosel nahraje se hodnota
			if(is_parameter)
			{
				if((i+1)>=count_arg)
				{
					throw std::runtime_error("No value for parameter: "+ std::string(array_arg[i]));
				}
				value = std::string(array_arg[i+1]);
				if(is_num_par)
				{
					std::string::size_type sz;
					switch(parameter)
					{
						case 1:
							data.ttl = (unsigned)std::stoi(value,&sz);
							break;
						case 3:
							data.capabilities = std::stoi(value,&sz);
							break;
					}
					if(value.substr(sz)!="")
					{
						throw std::runtime_error("Invalid value: " + value);
					}
				}
				else
				{
					switch(parameter)
					{
						case 2:
							if(value=="half")
							{
								data.duplex = 1;
							}
							else if (value=="full")
							{
								data.duplex = 2;
							}
							else
							{
								throw std::runtime_error("Invalid value: " + value);
							}
							break;
						case 4:
							data.software_version = value;
							break;
						case 5:
							data.device_id = value;
							break;
						case 6:
							data.platform = value;
							break;
						case 7:
							data.port_id = value;
							break;
						case 8:
							struct sockaddr_in sa;
							if(inet_pton(AF_INET, value.c_str(),&(sa.sin_addr))==0)
							{
								throw std::runtime_error("Invalid IPv4 address: " + value);
							}
							data.IPv4_address = value;
							break;
					}
				}
			}
			else
			{
				throw std::runtime_error("Wrong argument: " + std::string(array_arg[i]));
			}
		}
	}
	//nastaveni vychozich hodnot
	SetDefaultValues();
}

void ArgParser::SetDefaultValues()
{	//pokud jsou inicializacni hodnoty nastavuji se vychozi
	if(data.ttl == 0)
		data.ttl = 180;
	if(data.duplex==0)
		data.duplex = 2;
	if(data.capabilities==0)
		data.capabilities = 16;

	if(data.port_id==EMPTYSTRING)
	{
		data.port_id = data.interface;
	}
	if(data.device_id==EMPTYSTRING)
	{
		data.device_id = GetVersionSoft_DeviceID(1);
	}
	if(data.software_version==EMPTYSTRING)
	{
		data.software_version = GetVersionSoft_DeviceID();
	}
	if(data.platform==EMPTYSTRING)
	{
		data.platform = GetVersionSoft_DeviceID(2);
	}
	if(data.IPv4_address==EMPTYSTRING)
	{
		data.IPv4_address = GetIpv4AddressOfInterface(data.interface.c_str());
	}
}
//nejvyznamnejsi funkce v nastavovani vychozich hodnot
std::string ArgParser::GetVersionSoft_DeviceID(int vs_device_platform)
{
	struct utsname name;	
	uname(&name);	//vyuziti systemoveho volani

	if(vs_device_platform==1)
	{
		return std::string(name.nodename);
	}
	if(vs_device_platform==2)
	{
		return std::string(name.sysname);
	}

	std::string version;
	version.append(name.sysname);
	version.append(" ");
	version.append(name.nodename);
	version.append(" ");
	version.append(name.release);
	version.append(" ");
	version.append(name.version);
	version.append(" ");
	version.append(name.machine);
	version.append(" ");
	version.append(name.domainname);
	
	return version;
}

std::string ArgParser::GetIpv4AddressOfInterface(const char* interface)
{
	struct ifreq ifr;
	int fd = socket(AF_INET, SOCK_DGRAM, 0);

	ifr.ifr_addr.sa_family = AF_INET;

	std::strncpy(ifr.ifr_name,interface,IFNAMSIZ-1);
	ioctl(fd,SIOCGIFADDR,&ifr);
	close(fd);

	return std::string(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}

//get funkce
std::string ArgParser::GetInterface()
{
	return data.interface;
}
std::string ArgParser::GetSoftVersion()
{
	return data.software_version;
}
std::string ArgParser::GetPlatform()
{
	return data.platform;
}
std::string ArgParser::GetDeviceID()
{
	return data.device_id;
}
std::string ArgParser::GetPortID()
{
	return data.port_id;
}
std::string ArgParser::GetAddress()
{
	return data.IPv4_address;
}
unsigned int ArgParser::GetTTL_seconds()
{
	return data.ttl;
}
bool ArgParser::isDuplexFull()
{
	if(data.duplex==2)
		return true;
	return false;
}
int ArgParser::GetCapabilities()
{
	return data.capabilities;
}
bool ArgParser::isSendHello()
{
	return data.send_hello;
}