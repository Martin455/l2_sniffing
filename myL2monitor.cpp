/***
*
* ISA Projekt: Monitorovací nástroj protokolů CDP a LLDP
* Autor: Martin Prajka
*/

//Zakladni knihovny
#include <iostream>
#include <stdexcept>
#include <iomanip>
#include <sstream>
#include <csignal>

//Time funkce
#include <chrono>
//vlakna
#include <thread>

//moje hlavickove soubory
#include "arg_parser.h"
#include "packet_work.h"

//pouziti namespace namespace
using std::endl;
using std::cerr;
using std::cout;
using std::string;
using namespace std::chrono;

//konstanty
const char *FILTER = "ether[20:2] == 0x2000 or ether proto 0x88cc"; //zachytava pouze CDP a LLDP protokoly

//globalni promenne
u_char *packet_cdp = nullptr;
u_char *packet_lldp = nullptr;
uint len_cdp;
uint len_lldp;
pcap_t *handle;
bool sending = true;

std::thread sendThread;


auto last_time = high_resolution_clock::now();

//deklarace funkci
void sniff_init(const char*);
void sniffing_loop(u_char*,const struct pcap_pkthdr*,const u_char*);
void sendPacket(const char *dev, u_char *packet, uint length_packet);
void sendingPackets(const char *device);

void terminate(int);
/*Funkce Main
*************/
int main(int argc, char **argv)
{
	ArgParser parser(argc,argv);	//volani parsovani argumentu a vypis na jakem rozhrani a s jakym IP bude zpracovavat pakety (odesilat)
	signal(SIGINT,terminate);
	//vypis nazvu aplikace
	Functions::print_title(); 
	try	//blok pro parsovani argumentu prikazove radky
	{
    	parser.ParseData();		
	}
	catch(std::exception &e)
	{
		cerr << "App failed with argument exception, message:\n" << e.what() << endl << Functions::print_help() << endl;
		return 1;
	}

    try
    {	//hlavni blok programu
    	cout << "Sniffing on:\nInterface: " << parser.GetInterface() << "\nIP:        " << parser.GetAddress() << endl;
    	//tvoreni paketu
    	createCDP_packet(parser);
    	createLLDP_packet(parser);
    	sendThread = std::thread(sendingPackets, parser.GetInterface().c_str());	//vlastni vlakno pro odesilani
    	sniff_init(parser.GetInterface().c_str());

    	if(sendThread.joinable())
    		sendThread.join();	//synchronizace vlaken
    }
    catch(std::exception &e)
    {	//v pripade chyby synchronizace vlaken a uklizeni pameti
    	sending = false;
    	if (handle)
    		pcap_breakloop(handle);
    	if(sendThread.joinable())
    		sendThread.join();

    	destroyPacket(packet_cdp);
    	destroyPacket(packet_lldp);
    	cerr << "App failed with network exception message:\n" << e.what() << endl << Functions::print_help() << endl;   	
    	return 2;
    }
    //uklizeni pameti pro pakety
    destroyPacket(packet_cdp);
    destroyPacket(packet_lldp);
    return 0;
}

void sniff_init(const char *device)
{
	char errbuf[PCAP_ERRBUF_SIZE];
	bpf_u_int32 net_address;
	bpf_u_int32 mask_address;
	struct in_addr address;

	struct bpf_program fp;

	//zjisteni adresy site a masky
	pcap_lookupnet(device,&net_address,&mask_address,errbuf);
	address.s_addr = net_address;
	cout << "NET address: " << inet_ntoa(address);
	address.s_addr = mask_address;
	cout << " MASK address: " << inet_ntoa(address) << endl;
	cout << "---------------------------------------------------" << endl;
	
	//otevreni pro poslouchani na danem rozhrani
	handle = pcap_open_live(device,BUFSIZ,1,1000,errbuf);
	
	if (handle == nullptr)
		throw std::runtime_error("Failed open handle: try 'sudo'");
	//prelozeni filtru na CDP a LLDP pakety
	if (pcap_compile(handle,&fp,FILTER,0,0) == -1)
		throw std::runtime_error("Error with filter compile");
	//nastaveni filtru
	if (pcap_setfilter(handle,&fp) == -1)
		throw std::runtime_error("Failed setting filter");
	//spusteni nekonecne smycky pro naslouchani paketu
	if (pcap_loop(handle,-1,sniffing_loop,nullptr) == -1)
		throw std::runtime_error("Error with starting sniffing loop");
	//zavreni descriptoru
	pcap_close(handle);
}
//nekonecna smycka naslouchani na rozhrani
void sniffing_loop(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
	if (args!=nullptr)
		return;
	static unsigned count_of_packet = 1;	//pocitadlo paketu
	struct ether_header *e_head;
	u_char *data;
	uint16_t protocol;
	unsigned rest_len = 0;
	//cteni IP hlavicky
	e_head = (struct ether_header *)packet;
	protocol = ntohs(e_head->ether_type);	//zjisteni typu protokolu
	cout << "Packet No. " << count_of_packet << endl;
	cout << "Length packet: " << header->len << endl;
	//vypis mac adres
	cout << "MAC src: " << ether_ntoa((const struct ether_addr*)&e_head->ether_shost);
	cout << " MAC dst: " << ether_ntoa((const struct ether_addr*)&e_head->ether_dhost) << endl;

	cout << "Protocol: ";
	if (protocol==LLDP)		//zpracovani LLDP protokolu
	{
		data = (u_char *) packet+IP_OFFSET;	//nastaveni dat protokolu lldp
		cout << "LLDP" << endl;
		rest_len = header->len - IP_OFFSET;	//zbyvajici delka paketu

		while(rest_len!=0)
		{
			//pocatecni inicializace kazdeho tlv 
			bool valid_type = true;	
			uint tlv_type = ((uint)data[0] >> 1);
			uint length = Functions::convertBufferToInt(data,2);
			length = length & 0x01ff;
			data+=2;
			switch(tlv_type)
			{
				case 0x0001:
				{	//chassis pro vypis vetsinou mac adresy
					cout << "Chassis ID" << endl;
					uint subtype = (int)data[0];
					data++;
					switch(subtype)	//muze se vyskytovat vice typu je nutno osetrit
					{
						case 3:
							cout <<" Id Subtype: Portcomponent (3)" << endl;
							break; 
						case 4:
						{
							cout << " Id Subtype: MacAddress (4): " << std::hex;
							for(int i = 0; i < 6; ++i)
							{
								cout << (int)data[i];
								if (i<5)
								{
									cout << ":";
								}
							}
							cout << std::dec << endl;
							break;
						}
						case 5:
							cout << " Id Subtype: NetworkAddress (5)" << endl;
							break;
						case 6:
							cout << " Id Subtype: Interface name (6) " << Functions::convertBufferToString(data,length-1) << endl;
							break;
						default:
							cout << " IdSubtype: non support" << endl;
							break;
					}
					data--;
					break;
				}
				case 0x0002:
				{
					cout << "Port Subtype:" << endl;
					uint subtype = (int)data[0];
					data++;
					switch(subtype)
					{
						case 3:
							cout << "Id Subtype: MacAddress (3) " << std::hex;
							for(int i = 0; i < 6; ++i)
							{
								cout << (int)data[i];
								if(i<5)
								{
									cout << ":";
								}
							}
							cout << std::dec << endl;
							break;
						case 5:
							cout << " Id Subtype: Interface name (5) " << Functions::convertBufferToString(data,length-1) << endl;
							break;
						default:
							cout << "Id Subtype: non Support" << endl;
					}
					data--;	
					break;
				}
				case 0x0003:
					cout << "TTL: " << Functions::convertBufferToInt(data,length) << endl;
					break;
				case 0x0005:
					cout << "System Name: " << Functions::convertBufferToString(data,length) << endl;
					break;
				case 0x0006:
				{	
					string desc = Functions::convertBufferToString(data,length);
					std::stringstream ss;
					ss.str(desc);
					string item;
					cout << "System Description:" << endl;
					while(std::getline(ss,item))
					{
						cout << " " << item << endl;
					}
					break;
				}
				case 0x0004:
					cout << "Port Description: " << Functions::convertBufferToString(data,length) << endl;
					break;
				case 0x0007:	//zjistovani vlastnosti zarizeni kazdy bit se musi overit jako flag
				{
					uint capabilities = Functions::convertBufferToInt(data,length/2);
					data += 2;
					uint capab_enable = Functions::convertBufferToInt(data,length/2);
					data -= 2;
					//overovani
					cout << "Capabilities: " << endl;
					cout << " Cap: " << capabilities << endl;

					char const *name_array[]={"Other", "Repeater", "Bridge", "WLAN", "Router", "Telephone" , "DOCSIS", "Station only"};
					for(uint i=0;i<8;++i)
					{
						cout << "  " << name_array[i] << ": ";
						if((capabilities & (unsigned)(1<<i))==(unsigned)(1<<i))
							cout << "Capable" << endl;
						else
							cout << "Not capable" << endl;
					}
					//enable cap
					cout << " Enabled Cap: " << capab_enable << endl;
					for(uint i=0;i<8;++i)
					{
						cout << "  " << name_array[i] << ": ";
						if((capab_enable & (unsigned)(1<<i))==(unsigned)(1<<i))
							cout << "Capable" << endl;
						else
							cout << "Not capable" << endl;
					}				
					break;
				}
				default:
					valid_type = false;
			}
			if(valid_type)
			{
				cout << " TLV Type: " << tlv_type << endl;
				cout << " TLV Length: " << length << endl;
			}
			data += length;	//posun v datech
			rest_len -= (length + 2);	//zkraceni zbyvajici delky
		}
	}	
	else  //zpracovani CDP protokolu
	{
		//pocatecni inicializace pro zpracovani CDP paketu
		data = (u_char *) packet+CDP_OFFSET;	//cdp offset je jiny nez IP protoze ma logical link control 8 bytu
		cout << "CDP" << endl;
		cout << " Version: " << (int)data[0] << endl;
		cout << " TTL: " << (int)data[1] << endl;
		data += 2; //posun o 2 byty
		cout << " Checksum: 0x" << std::hex << Functions::convertBufferToInt(data,2) << std::dec << endl;
		data += 2;
		rest_len = header->len - (CDP_OFFSET+4);	//nastaveni zbyvajici delky paketu

		while(rest_len!=0)
		{
			//zpravoni tlv dat 
			bool valid_type = true;
			uint type = Functions::convertBufferToInt(data,2);
			data += 2;
			uint length = Functions::convertBufferToInt(data,2);
			data += 2;

			length -= 4;

			switch(type)	//rozdeleni podle typu zpravy
			{	//vypisy na stdout
				case 0x0001: //device
				{
					string deviceID = Functions::convertBufferToString(data, length);
					cout << "DeviceID: " << deviceID << endl;
					break;
				}
				case 0x0006: //platforma
				{
					string platform = Functions::convertBufferToString(data,length);
					cout << "Platform: " << platform << endl;
					break; 
				}
				case 0x000b: //duplex
				{
					uint duplex = Functions::convertBufferToInt(data,length);
					cout << "Duplex: " << ((duplex) ? "Full" : "Half") << endl;
					break;
				}
				case 0x0005: //verze softwaru
				{
					string version = Functions::convertBufferToString(data,length);
					std::stringstream ss;
					ss.str(version);
					string item;
					cout << "Software Version:" << endl;
					while(std::getline(ss,item))
					{
						cout << " " << item << endl;
					}
					break;
				}
				case 0x0004: //capabilities
				{	//maskovani jako u lldp
					uint capabilities = Functions::convertBufferToInt(data,length);
					cout << "Capabilities: " << capabilities << endl;
					char const *name_array[] = {"Router", "Transparent Bridge", "Source Route Bridge", "Switch", "Host", "IGMP capable", "Repeater"};
					for(uint i=0;i<7;++i)
					{
						cout << " " << name_array[i] << ": ";
						if((capabilities & (unsigned)(1<<i))==(unsigned)(1<<i))
							cout << "Yes" << endl;
						else
							cout << "No" << endl;
					}				
					break;
				}
				case 0x0003: //port ID
				{
					string portID = Functions::convertBufferToString(data,length);
					cout << "Port ID: " << portID << endl;
					break;
				}
				case 0x0002:	//IP adresa
				{
					u_char *tmp_data = data;
					cout << "Addresses:" << endl;
					uint num_of_address = Functions::convertBufferToInt(tmp_data,4);
					cout << " Number of Addresses: " << num_of_address << endl;
					tmp_data +=4;
					
					for(uint i = 0; i < num_of_address; i++)
					{
						tmp_data+=1; //preskoceni dat na delku protokolu
						uint protocol_len = Functions::convertBufferToInt(tmp_data,1);
						tmp_data+= protocol_len + 1;
						uint address_len = Functions::convertBufferToInt(tmp_data,2);
						tmp_data+=2;
					
						if (address_len==4) //IpV4 adresa
						{
							cout << " IP Address: ";
							cout << (int)tmp_data[0] << "." << (int)tmp_data[1] << "." << (int)tmp_data[2] << "." << (int)tmp_data[3] << endl;
						}
					}
					break;
				}
				default:
					valid_type = false;

			}
			if(valid_type)	//vypis delky a cislo typu
			{
				cout << " Type: 0x" << std::setfill('0') << std::setw(4) << std::hex << type << std::dec << endl;
				cout << " Length: " << length+4 << endl;
			}
			data += length;	//posuv v datech
			rest_len -= length + 4;	//odecteni zbyvajici delky
		}
	}
	cout << "---------------------------------------------" << endl; //logicke oddeleni
	count_of_packet++;		//increment cisla paketu
	cout << std::flush;		//splachnuti bufferu, pro zapis do souboru pomoci >
}
//funkce odesilaciho vlakna programu
void sendingPackets(const char *device)
{	
	while(sending)
	{
		std::this_thread::sleep_for(seconds(1));	//spanek vlakna 1s aby nebyl procesor tak vytizen
		auto currTime = high_resolution_clock::now();
		duration<double> timespan = duration_cast<duration<double>>(currTime - last_time);
		if (timespan.count() > 30.0)	//odeslani paketu po +- 30 sekundach
		{
			sendPacket(device,packet_cdp,len_cdp);
    		sendPacket(device,packet_lldp,len_lldp);
    		last_time = currTime;	//nastaveni casu na posledni odeslani
		}
	}
}

void sendPacket(const char *dev, u_char *packet, uint size_packet)
{	//posilani paketu na L2 vrstve
	pcap_t *handle;	
	char errbuf[PCAP_ERRBUF_SIZE];
	//otevreni spojeni descriptoru na rozhrani
	handle = pcap_open_live(dev,BUFSIZ,1,1000,errbuf);
	if (handle==nullptr)
		throw std::runtime_error("Failed pcap open handle for send");
	//poslani paketu
	if (pcap_sendpacket(handle,packet,size_packet)!=0)
		throw std::runtime_error("failed sending");
	pcap_close(handle);	//zavreni spojeni
}

//po signalu SIGINT zrusi se obe smycky
void terminate(int sig)
{
	if(!sig)	//neni potreba osetreni aby nebyl warning
		return;

	pcap_breakloop(handle);
	sending = false;
}